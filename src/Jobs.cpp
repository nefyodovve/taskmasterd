#include <iostream>
#include <cstring>

#include "yaml-cpp/yaml.h"
#include "Jobs.h"
#include "Logger.h"

Job::Job()
{}

Job::Job(const Job& j)
{
    //logger.info("copying Job");
    name = j.name;
    cmd = j.cmd;
    numprocs = j.numprocs;
    working_dir = j.working_dir;
    autostart = j.autostart;
    restart = j.restart;
    exit_codes = j.exit_codes;
    start_retries = j.start_retries;
    start_time = j.start_time;
    stop_time = j.stop_time;
    stop_signal = j.stop_signal;
    stdout_path = j.stdout_path;
    stderr_path = j.stderr_path;
    umask = j.umask;
    env = j.env;
    status = j.status;
}

bool Job::isAlive()
{
    switch (status) {
    case Status::Starting:
    case Status::Running:
    case Status::Stopping:
        return true;
    default:
        return false;
    }
}

std::string Job::strRestart() const
{
    switch (restart) {
    case Restart::Always:
        return "always";
    case Restart::Never:
        return "never";
    case Restart::UnexpectedExits:
        return "unexpected";
    }
}

std::string Job::strStatus() const
{
    switch (status) {
    case Status::Starting:
        return "starting";
    case Status::Running:
        return "running";
    case Status::Stopping:
        return "stopping";
    case Status::Stopped:
        return "stopped";
    case Status::Exited:
        return "exited";
    case Status::Crashed:
        return "crashed";
    case Status::Failed:
        return "failed";
    }
}

static std::string vecint2str(const std::vector<int> &vec)
{
    std::string res;
    if (vec.empty())
        return "(empty)";
    for (size_t i = 0; i < vec.size(); ++i) {
        res += std::to_string(vec[i]);
        if (i != vec.size() - 1)
            res += ", ";
    }
    return res;
}


static std::string vecstr2str(const std::vector<std::string> &vec)
{
    std::string res;
    if (vec.empty())
        return "(empty)";
    for (size_t i = 0; i < vec.size(); ++i) {
        res += vec[i];
        if (i != vec.size() - 1)
            res += ", ";
    }
    return res;
}

bool Jobs::fill(const std::string &path)
{
    YAML::Node cfg = YAML::LoadFile(path);
    YAML::Node programs = cfg["programs"];
    if (programs.IsMap()) {
        for (YAML::const_iterator it = programs.begin(); it != programs.end(); ++it) {
            try {
                std::string name = it->first.as<std::string>();
                logger.info("Loading '%s':", name.c_str());
                jobs.push_back({});
                Job &job = jobs.back();
                job.name = name;
                YAML::Node program = it->second;
                if (program.IsMap()) {
                    if (!program["cmd"].IsDefined()) {
                        logger.error("No cmd. Skipping");
                        continue ;
                    }
                    job.cmd = program["cmd"].as<std::string>();
                    logger.info("\tcmd: '%s'", job.cmd.c_str());
                    if (program["numprocs"].IsDefined()) {
                        job.numprocs = program["numprocs"].as<int>();
                        logger.info("\tnumproc: %d", job.numprocs);
                    }
                    if (program["workingdir"].IsDefined()) {
                        job.working_dir = program["workingdir"].as<std::string>();
                        logger.info("\tworking_dir: '%s'", job.working_dir.c_str());
                    }
                    if (program["autostart"].IsDefined()) {
                        job.autostart = program["autostart"].as<bool>();
                        logger.info("\tautostart: %s", job.autostart ? "true" : "false");
                    }
                    if (program["autorestart"].IsDefined()) {
                        std::string restart = program["autorestart"].as<std::string>();
                        if (restart == "never") {
                            job.restart = Job::Restart::Never;
                            logger.info("\trestart: Never");
                        } else if (restart == "always") {
                            job.restart = Job::Restart::Always;
                            logger.info("\trestart: Always");
                        } else {
                            job.restart = Job::Restart::UnexpectedExits;
                            logger.info("\trestart: UnexpectedExits");
                        }
                    }
                    if (program["exitcodes"].IsDefined()) {
                        if (program["exitcodes"].IsSequence())
                            job.exit_codes = program["exitcodes"].as<std::vector<int>>();
                        else
                            job.exit_codes.emplace_back(program["exitcodes"].as<int>());
                        logger.info("\texitcodes: %s", vecint2str(job.exit_codes).c_str());
                    } else {
                        job.exit_codes.push_back(0);
                    }
                    if (program["startentries"].IsDefined()) {
                        job.start_retries = program["startretries"].as<int>();
                        logger.info("\tstart_retries: %d", job.start_retries);
                    }
                    if (program["starttime"].IsDefined()) {
                        job.start_time = program["starttime"].as<int>();
                        logger.info("\tstart_time: %d", job.start_time);
                    }
                    if (program["stoptime"].IsDefined()) {
                        job.stop_time = program["stoptime"].as<int>();
                        logger.info("\tstop_time: %d", job.stop_time);
                    }
                    if (program["stopsignal"].IsDefined()) {
                        job.stop_signal = program["stopsignal"].as<int>();
                        logger.info("\tstop_signal: %d (%s)", job.stop_signal, strsignal(job.stop_signal));
                    }
                    if (program["stdout"].IsDefined()) {
                        job.stdout_path = program["stdout"].as<std::string>();
                        logger.info("\tstdout: '%s'", job.stdout_path.c_str());
                    }
                    if (program["stderr"].IsDefined()) {
                        job.stderr_path = program["stderr"].as<std::string>();
                        logger.info("\tstderr: '%s'", job.stderr_path.c_str());
                    }
                    if (program["umask"].IsDefined()) {
                        std::string umask_str = program["umask"].as<std::string>();
                        job.umask = std::stoi(umask_str, nullptr, 0);
                        logger.info("\tumask: %s (%d)", umask_str.c_str(), job.umask);
                    }
                    if (program["env"].IsDefined()) {
                        if (program["env"].IsMap()) {
                            for (YAML::const_iterator it_env = program["env"].begin(); 
                                    it_env != program["env"].end(); ++it_env) {
                                job.env.push_back(it_env->first.as<std::string>() + "=" + 
                                                it_env->second.as<std::string>());
                            }
                        }
                        logger.info("\tenv: %s", vecstr2str(job.env).c_str());
                    }
                    if (job.numprocs > 1) {
                        for (int i = 0; i < job.numprocs - 1; ++i) {
                            jobs.push_back(job);
                        }
                    }
                } else {
                    logger.error("'%s' is not a map. Quit.", name.c_str());
                    return false;
                }
            } catch (YAML::Exception &ex) {
                if (ex.mark.line >= 0 && ex.mark.column >=0) 
                    logger.warning("Error while parsing configuration:%d:%d: %s", 
                        ex.mark.line + 1, ex.mark.column, ex.msg.c_str());
                else
                    logger.warning("Error while parsing configuration: %s", ex.msg.c_str());
                continue ;
            }
        }
    }
    
    return true;
}
