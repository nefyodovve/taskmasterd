#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include "taskmaster.h"
#include "Server.h"
#include "Logger.h"
#include "JobsManager.h"

#define BUFSIZE     100
#define BACKLOG     1

/*

-> "start nginx"
	' ' -> '\n'
	len("start nginx") = 11 = 0xB

<- [0][0][0][0xB][s][t][a][r][t][\n][n][g][i][n][x]

*/
int encode_message(const char *msg, char **p_buf, int *p_size)
{
	if (!msg || !p_buf || !p_size)
		return -1;
	char *buf;
    int in_quote = 0;
	int msgsize = strlen(msg);
	int size = msgsize + sizeof(int) + 1;
	buf = (char *)malloc(sizeof(*buf) * (size));
	memcpy(buf, &msgsize, sizeof(int));
	int j = sizeof(int);
	for (int i = 0; i < msgsize; ++i, ++j) {
        if (msg[i] == '"') {
            in_quote = !in_quote;
        }
        if (in_quote)
            buf[j] = msg[i];
        else
		    buf[j] = (msg[i] == ' ' ? '\n' : msg[i]);
	}
	buf[j] = '\n';
	*p_buf = buf;
	*p_size = size;
	return 0;
}

static int decode_message(const char *buf, char ***p_msgv, int *p_msgc)
{
    char **msgv;
    int len;
    int msgc = 0;
    int i;
    int j;
    int word_size;

    if (!buf || !p_msgv || !p_msgc)
        return -1;
    memcpy(&len, buf, sizeof(int));
    for (i = sizeof(int); i <= len + (int)sizeof(int); ++i) {
        if (buf[i] == '\n')
            ++msgc;
    }
    msgv = (char **)malloc(sizeof(*msgv) * (msgc + 1));
    i = sizeof(int);
    for (j = 0; j < msgc; ++j) {
        word_size = i;
        while (buf[i] != '\n')
            ++i;
        word_size = i - word_size;
        msgv[j] = (char *)malloc(sizeof(**msgv) * (word_size + 1));
        memcpy(msgv[j], &buf[i - word_size], word_size);
        msgv[j][word_size] = '\0';
        ++i;
    }
    msgv[j] = NULL;
    *p_msgv = msgv;
    *p_msgc = msgc;
    return 0;
}

bool Server::start()
{
    struct sockaddr_un addr = {};
    int sfd, cfd;
    ssize_t num_read;
    char buf[BUFSIZE] = {};

    sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sfd == -1) {
        logger.perror("Can't create socket");
        return false;
    }
    logger.info("Socket path: %s", SOCKETPATH);
    if (remove(SOCKETPATH) == -1 && errno != ENOENT) {
        logger.perror("Can't delete old socket");
        return false;
    }
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SOCKETPATH, sizeof(addr.sun_path) - 1);
    if (bind(sfd, reinterpret_cast<struct sockaddr *>(&addr), sizeof(struct sockaddr_un)) == -1) {
        logger.perror("Can't bind socket");
        return false;
    }
    if (listen(sfd, BACKLOG) == -1) {
        logger.perror("Can't start listening on socket");
        return false;
    }
    logger.info("Server started");
    while (true) {
        cfd = accept(sfd, nullptr, nullptr);
        if (cfd == -1) {
            logger.perror("Error in accept()");
            return false;
        }
        num_read = read(cfd, buf, BUFSIZE);
        if (num_read == -1) {
            logger.perror("Error in read()");
            return false;
        }
        char **msgv = nullptr;
        int msgc;
        //logger.info("numRead = %d", num_read);
        decode_message(buf, &msgv, &msgc);
        std::string income;
        std::string reply;
        for (int i = 0; i < msgc; ++i) {
            income += std::string(msgv[i]) + (i == msgc - 1 ? "" : " ");
        }
        if (!msgv || !msgv[0] || msgc < 1) {
            logger.error("Invalid command");
            continue ;
        } else {
            logger.info("Get command: '%s'", income.c_str());
        }
        
        if (strcmp(msgv[0], "start") == 0) {
            if (msgc < 2) {
                reply = "No argument";
            } else {
                manager.executeCommand(JobsManager::Command::Start, msgv[1], reply);
            }
        } else if (strcmp(msgv[0], "stop") == 0) {
            if (msgc < 2) {
                reply = "No argument";
            } else {
                manager.executeCommand(JobsManager::Command::Stop, msgv[1], reply);
            }
        } else if (strcmp(msgv[0], "restart") == 0) {
            if (msgc < 2) {
                reply = "No argument";
            } else {
                manager.executeCommand(JobsManager::Command::Restart, msgv[1], reply);
            }
        } else if (strcmp(msgv[0], "status") == 0) {
            Jobs jobs = manager.getJobs();
            reply = buildStatusReply(jobs, msgc > 1 ? msgv[1] : "");
        } else if (strcmp(msgv[0], "reload") == 0) {
            manager.executeCommand(JobsManager::Command::Reload, msgv[1], reply);
        } else if (strcmp(msgv[0], "shutdown") == 0) {
            raise(SIGTERM);
        } else {
            reply = "Unknown command";
        }
        logger.info("Reply: '%s'", reply.c_str());
        char *bufwrite;
        int sizewrite;
        encode_message(reply.c_str(), &bufwrite, &sizewrite);
        if (write(cfd, bufwrite, sizewrite) == -1)
            logger.perror("socket write");
        if (close(cfd) < 0) {
            logger.perror("Error in close()");
            return false;
        }

        //usleep(100000);
    }
    return true;
}

std::string Server::buildStatusReply(const Jobs &jobs, const std::string &name)
{
    std::string res;
    std::string after_n;
    int n = 0;

    for (size_t i = 0; i < jobs.jobs.size(); ++i) {
        const Job &job = jobs.jobs[i];
        if (name.empty() || job.name == name) {
            after_n += "\"" + job.name + "\" ";
            after_n += "\"" + job.cmd + "\" ";
            after_n += std::to_string(job.pid) + " ";
            after_n += std::to_string(job.start_retries) + " ";
            after_n += "\"" + job.strStatus() + " ";
            after_n += std::to_string(job.status_code) + "\" ";
            n++;
        }
    }
    if (n) {
        after_n.pop_back();
        res = "ok " + std::to_string(n) + " " + after_n;
    } else {
        res = "ok";
    }
    return res;
}

#undef BACKLOG
#undef BUFSIZE
