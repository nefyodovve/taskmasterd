/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   taskmaster.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmelisan <gmelisan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/28 05:01:40 by gmelisan          #+#    #+#             */
/*   Updated: 2020/09/28 05:01:44 by gmelisan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TASKMASTER_H
#define TASKMASTER_H

#include <string>

#define PROGRAMNAME             "taskmasterd"
#define PROGRAMVERSION          "0.1.0"
#define DEFAULT_CONFIG_PATH     "example.yml"
#define SOCKETPATH              "/tmp/taskmaster.sock"
#define LOGPATH                 "taskmasterd.log"

extern std::string config_path;

#endif
